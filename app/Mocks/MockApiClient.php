<?php

namespace App\Mocks;

use App\Contracts\EcommerceApiContract;

class MockApiClient implements EcommerceApiContract
{
    public function searchOrders(int $count, string $dateFrom, string $dateTo): array
    {
        // Simula dati di risposta per la ricerca degli ordini
        return [
            [
                "orderItems" => [
                    [
                        "barcode" => "4062345021658",
                        "price" => 24300,
                        "cost" => 24300,
                        "tax_perc" => 20,
                        "tax_amt" => 4050,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ],
                    [
                        "item_sid" => "4062345067851",
                        "price" => 37800,
                        "cost" => 37800,
                        "tax_perc" => 20,
                        "tax_amt" => 63000,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ]
                ],
                "orderId" => "PP0400104913",
                "phone" => "+79620230303",
                "shipping_status" => "not sent",
                "shipping_price" => 1000,
                "shipping_payment_status" => "paid",
                "payment_status" => "paid"
            ],
            [
                "orderItems" => [
                    [
                        "barcode" => "4062345021658",
                        "price" => 24300,
                        "cost" => 24300,
                        "tax_perc" => 20,
                        "tax_amt" => 4050,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ],
                    [
                        "item_sid" => "4062345067851",
                        "price" => 37800,
                        "cost" => 37800,
                        "tax_perc" => 20,
                        "tax_amt" => 63000,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ]
                ],
                "orderId" => "PP040023123",
                "phone" => "+79620230303",
                "shipping_status" => "not sent",
                "shipping_price" => 1000,
                "shipping_payment_status" => "not paid",
                "payment_status" => "not paid"
            ]
        ];
    }

    public function getOrder(string $orderId): array
    {
        // Simula dati di risposta per il dettaglio di un ordine specifico
        return [
            "orderItems" => [
                [
                    "barcode" => "4062345021658",
                    "price" => 24300,
                    "cost" => 24300,
                    "tax_perc" => 20,
                    "tax_amt" => 4050,
                    "quantity" => 1,
                    "tracking_number" => "1Z05V36Y7951053268",
                    "canceled" => "N",
                    "shipped_status_sku" => "not sent"
                ]
            ],
            "orderId" => "PP0400104913",
            "phone" => "+79620230303",
            "shipping_status" => "not sent",
            "shipping_price" => 1000,
            "shipping_payment_status" => "paid",
            "payment_status" => "paid"
        ];
    }
}
