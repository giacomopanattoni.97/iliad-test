<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'barcode',
        'item_sid',
        'price',
        'cost',
        'tax_perc',
        'tax_amt',
        'quantity',
        'tracking_number',
        'canceled',
        'shipped_status_sku'
    ];
}
