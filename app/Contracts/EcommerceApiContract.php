<?php

namespace App\Contracts;

interface EcommerceApiContract
{
    public function searchOrders(int $count, string $dateFrom, string $dateTo): array;

    public function getOrder(string $orderId): array;
}
