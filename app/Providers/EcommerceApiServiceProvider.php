<?php

namespace App\Providers;

use App\Contracts\EcommerceApiContract;
use App\Mocks\MockApiClient;
use App\Repositories\OrderRepository;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class EcommerceApiServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EcommerceApiContract::class, MockApiClient::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
    }
}
