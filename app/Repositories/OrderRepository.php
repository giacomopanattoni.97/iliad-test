<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class OrderRepository implements OrderRepositoryInterface
{
    public function saveOrder(array $orderData): Order
    {
        return DB::transaction(function () use ($orderData) {

            $order = Order::updateOrCreate(
                ['order_id' => $orderData['orderId']],
                [
                    'phone' => $orderData['phone'],
                    'shipping_status' => $orderData['shipping_status'],
                    'shipping_price' => $orderData['shipping_price'],
                    'shipping_payment_status' => $orderData['shipping_payment_status'],
                    'payment_status' => $orderData['payment_status']
                ]
            );

            foreach ($orderData['orderItems'] as $itemData) {
                $this->saveOrderItem($itemData, $order->id);
            }

            return $order;
        });
    }

    public function saveOrderItem(array $itemData, int $orderId): void
    {
        OrderItem::updateOrCreate(
            [
                'order_id' => $orderId,
                'barcode' => $itemData['barcode'] ?? null,
                'item_sid' => $itemData['item_sid'] ?? null
            ],
            [
                'price' => $itemData['price'],
                'cost' => $itemData['cost'],
                'tax_perc' => $itemData['tax_perc'],
                'tax_amt' => $itemData['tax_amt'],
                'quantity' => $itemData['quantity'],
                'tracking_number' => $itemData['tracking_number'],
                'canceled' => $itemData['canceled'],
                'shipped_status_sku' => $itemData['shipped_status_sku']
            ]
        );
    }

    public function getOrderById($id): ?Order
    {
        return Order::find($id);
    }

    public function getOrdersWithFilters($filters = []): Collection
    {
        $query = Order::query();

        /* if (!empty($filters['date_from'])) {
            $query->where('created_at', '>=', $filters['date_from']);
        } */

        return $query->get();
    }
}
