<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    public function saveOrder(array $orderData): Order;
    public function saveOrderItem(array $itemData, int $orderId): void;
    public function getOrderById(int $id): ?Order;
    public function getOrdersWithFilters(array $filters = []): Collection;
}
