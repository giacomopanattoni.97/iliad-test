<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function show(int $id)
    {
        $order = $this->orderRepository->getOrderById($id);
        if (!$order) {
            return response()->json(['message' => 'Order not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($order);
    }

    public function index(Request $request)
    {
        $filters = $request->only(['date_from', 'date_to']);
        $orders = $this->orderRepository->getOrdersWithFilters($filters);

        return response()->json($orders);
    }
}
