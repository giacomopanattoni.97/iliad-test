<?php

namespace App\Validation;

class OrderValidationRules
{
    public static function getRules()
    {
        return [
            'orderId' => 'required|string',
            'phone' => 'required|string',
            'shipping_status' => 'required|string',
            'shipping_price' => 'required|numeric',
            'shipping_payment_status' => 'required|string',
            'payment_status' => 'required|string',
            'orderItems' => 'required|array',
            'orderItems.*.barcode' => 'nullable|string',
            'orderItems.*.item_sid' => 'nullable|string',
            'orderItems.*.price' => 'required|numeric',
            'orderItems.*.cost' => 'required|numeric',
            'orderItems.*.tax_perc' => 'required|integer',
            'orderItems.*.tax_amt' => 'required|numeric',
            'orderItems.*.quantity' => 'required|integer',
            'orderItems.*.tracking_number' => 'required|string',
            'orderItems.*.canceled' => 'required|string',
            'orderItems.*.shipped_status_sku' => 'required|string',
        ];
    }
}
