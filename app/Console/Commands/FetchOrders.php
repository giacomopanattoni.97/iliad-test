<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\OrderService;

class FetchOrders extends Command
{
    protected $signature = 'orders:fetch';
    protected $description = 'Fetch orders from external API and store them in the database';


    public function __construct(protected OrderService $orderService)
    {
        parent::__construct();
    }

    public function handle()
    {
        $dateFrom = now()->format('Ymd');
        $dateTo = now()->format('Ymd');
        $count = 20;

        $this->orderService->fetchAndStoreOrders($count, $dateFrom, $dateTo);
        $this->info('Orders fetched and stored successfully.');
    }
}
