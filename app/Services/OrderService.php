<?php

namespace App\Services;

use App\Contracts\EcommerceApiContract;
use App\Repositories\OrderRepository;
use App\Validation\OrderValidationRules;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class OrderService
{

    public function __construct(
        protected EcommerceApiContract $apiClient,
        protected OrderRepository $orderRepository
    ) {
        $this->apiClient = $apiClient;
    }

    public function fetchAndStoreOrders(int $count, string $dateFrom, string $dateTo): void
    {
        $orders = $this->apiClient->searchOrders($count, $dateFrom, $dateTo);
        foreach ($orders as $orderData) {
            $validatedOrder = $this->validateOrder($orderData);
            $this->orderRepository->saveOrder($validatedOrder);
        }
    }

    protected function validateOrder(array $orders): array
    {
        $validator = Validator::make($orders, OrderValidationRules::getRules());
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        return $validator->validated();
    }
}
